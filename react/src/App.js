import Demo from './components/Demo'

const App = () => (
  <>
    <h3>api-rate-limit demo</h3>
    <Demo />
  </>
)

export default App
