const ResponseDisplay = ({
  responseType,
  prefix
}) => {
  return responseType.map((res, idx) => {
    return (
      <li
        key={`${prefix}:${idx}`}
        className={res.status && 'alert'}
      >
        {JSON.stringify(res)}
      </li>
    )
  })
}

export default ResponseDisplay
