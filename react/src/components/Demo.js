import { useState } from 'react'
import axios from 'axios'

import ResponseDisplay from './ResponseDisplay'

const ENDPOINT = 'http://localhost:8082/demo'
const ACTIVE_HEADERS = {
  basic: '',
  groupA: 'dev-group-a',
  groupB: 'dev-group-b'
}
const TOO_MANY_REQUESTS = {
  status: 'Too Many Requests!'
}

const Demo = () => {
  const [basicResponse, setBasicResponse] = useState([])
  const [groupAResponse, setGroupAResponse] = useState([])
  const [groupBResponse, setGroupBResponse] = useState([])

  const STATE_MAP = {
    [ACTIVE_HEADERS.groupA]: [groupAResponse, setGroupAResponse],
    [ACTIVE_HEADERS.groupB]: [groupBResponse, setGroupBResponse],
    [ACTIVE_HEADERS.basic]: [basicResponse, setBasicResponse]
  }

  const demoCall = ({ devHeader }) => {
    const header = devHeader
      ? {
          headers: {
            'dev-header': devHeader
          }
        }
      : {}

    axios.get(ENDPOINT, header)
      .then(res => {
        const { data } = res
        if (data) {
          const stateArr = STATE_MAP[devHeader]
          const stateUpdate = stateArr[0].concat(data)
          stateArr[1](stateUpdate)
        }
      })
      .catch(err => {
        if (err.response.data === 'Too Many Requests') {
          const stateArr = STATE_MAP[devHeader]
          const stateUpdate = stateArr[0].concat(TOO_MANY_REQUESTS)
          stateArr[1](stateUpdate)
        } else {
          console.error('error:', err)
        }
      })
  }

  return (
    <>
      <table>
        <tbody>
          <tr>
            <th>basic <span>[/demo]</span></th>
            <th>group-a <span>[/demo]</span></th>
            <th>group-b <span>[/demo]</span></th>
          </tr>
          <tr>
            <td>
              <ul>
                <li>limit: 3 points per 15 seconds</li>
                <li>each request consumes 1 point</li>
              </ul>
            </td>
            <td>
              <ul>
                <li>limit: 20 points per 20 seconds</li>
                <li>each request consumes 5 points</li>
                <li>'dev-header' sent as "group-a"</li>
              </ul>
            </td>
            <td>
              <ul>
                <li>limit: 20 points per 20 seconds</li>
                <li>each request consumes 1 point</li>
                <li>'dev-header' sent as "group-b"</li>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
      <table>
        <tbody>
          <tr className='stick'>
            <th>
              <button
                onClick={() => demoCall({ devHeader: ACTIVE_HEADERS.basic })}
              >
                call-basic
              </button>
            </th>
            <th>
              <button
                onClick={() => demoCall({ devHeader: ACTIVE_HEADERS.groupA })}
              >
                call-group-a
              </button>
            </th>
            <th>
              <button
                onClick={() => demoCall({ devHeader: ACTIVE_HEADERS.groupB })}
              >
                call-group-b
              </button>
            </th>
          </tr>
          <tr>
            <td>
              <ul>
                <ResponseDisplay responseType={basicResponse} prefix='ba' />
              </ul>
            </td>
            <td>
              <ul>
                <ResponseDisplay responseType={groupAResponse} prefix='ga' />
              </ul>
            </td>
            <td>
              <ul>
                <ResponseDisplay responseType={groupBResponse} prefix='gb' />
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
    </>
  )
}

export default Demo
