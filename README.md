### pre-reqs
```
docker-compose [tested with v2.4.1]
docker [tested with 20.10.14]
```

### quickstart
```
docker-compose up
```

### default ports
api: 8082
web: 3002
redis: 6379

### demo
visit `http://localhost:3002` for ui demo
requests are regulated by allocating `points` over a reset duration (time in seconds)
- call-basic cta will run get on `:8082/demo` (max 3 points per 15 seconds by req.ip [each request consumes 1 point])
- call-group-a cta will run get on `:8082/demo` with `"dev-group-a"` as `dev-header` header param (max 20 points per 20 seconds by header param [each request consumes 5 points... i.e max of 4 requests/15sec])
- call-group-b cta will run get on `:8082/demo` with `"dev-group-b"` as `dev-header` header param (max 20 points per 20 seconds by header param [each request consumes 1 point... i.e. max 20 requests/20sec])

we can also limit a single route split into two groups that consume from the same point pool (i.e. group-a could consume 5 points per request and group-b 1 point... if group-a consumes 4 requests and in the same 20 seconds group-b tries to consume 1 after it will lock group-b's request )

### rate-limiter-flexible
```
yarn add rate-limiter-flexible -S
```