const express = require('express')
const router = express.Router()
const RAND = Math.random() // for testing vs load balancer
const demoResponse = {
  id: 0,
  message: 'demo',
  dev: 'babayaga'
}

router.get('/', (req, res) => {
  console.log(`returning posts from ${RAND}`)
  const randNum = Math.floor(Math.random() * 100)

  const response = Object.assign({}, demoResponse, {
    message: `${demoResponse.message}:${randNum}`
  })

  res.json(response)
})

module.exports = router
