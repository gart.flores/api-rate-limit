const redis = require('ioredis')
const { RateLimiterRedis } = require('rate-limiter-flexible')

const redisClient = redis.createClient({
  host: 'redis',
  port: 6379
})

redisClient.set('dev', 3)

redisClient.on('error', err => {
  console.error(err)
})

redisClient.on('connect', () => {
  console.log('redisClient connected')
})

redisClient.on('ready', () => {
  console.log('redisClient ready')
})

const basicRateLimiter = new RateLimiterRedis({
  storeClient: redisClient,
  keyPrefix: 'some-key-prefix',
  points: 3, // x requests
  duration: 15 // per x second(s) (refresh)
})

const groupRateLimiter = new RateLimiterRedis({
  storeClient: redisClient,
  keyPrefix: 'group',
  points: 20, // x requests
  duration: 20 // per x second(s) (refresh)
})

// demo map of active groups and the point values they consume per request i.e. stored on mongo
const activeGroups = {
  'dev-group-a': 5,
  'dev-group-b': 1
}

const rateLimiterMiddleware = (req, res, next) => {
  const consumeStr = `${req.ip.replaceAll('.', '-')}`

  const devHeader = req.headers['dev-header']
  const pointsToConsume = activeGroups[devHeader]

  if (pointsToConsume) {
    console.info(`api consuming [${devHeader}] by ${pointsToConsume} points`)

    groupRateLimiter.consume(devHeader, pointsToConsume)
      .then(() => {
        next()
      })
      .catch(() => {
        res.status(429).send('Too Many Requests')
      })
  } else {
    console.info(`api consuming [${consumeStr}]`)

    basicRateLimiter.consume(consumeStr)
      .then(() => {
        next()
      })
      .catch(() => {
        res.status(429).send('Too Many Requests')
      })
  }
}

module.exports = rateLimiterMiddleware
