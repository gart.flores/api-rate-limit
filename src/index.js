const express = require('express')
const cors = require('cors')
const app = express().use(cors())
const port = 8082
const routes = require('./routes')
const rateLimiterRedisMiddleware = require('./middleware/rateLimiterRedis')
const http = require('http').createServer(app)

app.use('/demo', rateLimiterRedisMiddleware, routes)

http.listen(port, '0.0.0.0', () => {
  console.log(`api listening at http://localhost:${port}`)
})
